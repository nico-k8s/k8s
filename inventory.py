#!/usr/bin/python3

import json
import libvirt

inventory = {
    "_meta": {
        "hostvars": {}
    },
    "all": {
        "children": ["libvirt_vm"]
    },
    "libvirt_vm": {
        "hosts": [],
    }
}

def recuperer_liste_nom_instance():
    libvirt_uri = "qemu:///system"
    conn = libvirt.open(libvirt_uri)

    list_domain = conn.listAllDomains()
    liste_nom_instance = []

    for index in list_domain:
        liste_nom_instance.append(index.name())

    conn.close()
    
    return liste_nom_instance

def recuperer_adresse_ip(nom_instance):
    libvirt_uri = "qemu:///system"
    conn = libvirt.open(libvirt_uri)
    instance = nom_instance

    domains = conn.lookupByName(instance)
    ifaces = domains.interfaceAddresses(libvirt.VIR_IP_ADDR_TYPE_IPV4)

    for value in ifaces.values():
        ip_adress = value["addrs"][0]["addr"]

    conn.close()

    return ip_adress

liste_nom_instance = recuperer_liste_nom_instance()

for nom_instance in liste_nom_instance:
    ip_adress = recuperer_adresse_ip(nom_instance)
    inventory["libvirt_vm"]["hosts"].append(ip_adress)

print(json.dumps(inventory, indent=2))


