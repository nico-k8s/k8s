variable "instance" {
  type = map
  default = {
    name = "k8s_1"
    nombre_instance = 6
    ip = "20.0.0"
    ssh_public_key = "ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBADI7xPJjC4iY27pijO9Kw/UKlzsznEDtyuZ4qFYxZ+QrKEsrzJ2Q2HsU0aE2UuB12GzV69Msboxc5pwpc/8zvZLRAHiNda8XvNOHVTtgpPNLYDgQ16TurUFFzTzV5vyrgWdvvWl8VY2WR0zz0fAb5RV9419Q+uNwjLv/ecqgg3Msu3s2A== ndf@ndf-tuxedo"
    mot_de_passe = "1dc44d23e82c6514556cdf51c58c2c0486caeabb6362080bb3d1359d3d76b9e7a58c24e51959a0d894d7ecd904ae7df1a3c94a47f72013e83e5f318bb05a5ab1"
    size = 50 # Taille du volume en Gb
    memory = 8192
    vcpu   = 5
  }
}

# Le provider est récuperer sur le réseau
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.1"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

# Création d'un network
resource "libvirt_network" "k8s_network" {
  name = "${var.instance.name}_vm_network"
  addresses = [
    "${var.instance.ip}.0/16"
    ]
  mode = "nat"
  dhcp {
    enabled = true
  }
}

# Création d'une pool
resource "libvirt_pool" "k8s_pool" {
  name = "${var.instance.name}_pool"
  type = "dir"
  path = "/var/lib/libvirt/${var.instance.name}_image_pool"
}

module "instance" {
  source = "gitlab.com/nico-terraform/instance/local"
  version = "2.0.0"
  name = var.instance.name
  nombre_instance = var.instance.nombre_instance
  ip = var.instance.ip
  ssh_public_key = var.instance.ssh_public_key
  mot_de_passe = var.instance.mot_de_passe
  size = var.instance.size
  memory = var.instance.memory
  vcpu = var.instance.vcpu
  network_id = libvirt_network.k8s_network.id
  pool = libvirt_pool.k8s_pool.name  
}

resource "null_resource" "inventory_init" {
  provisioner "local-exec" {
    command = "ansible-playbook -i \"localhost,\" -c local --tags wait initialisation_provisioning.yaml"
  }
  depends_on = [
    module.instance
  ]
}

resource "null_resource" "provisionning_init" {
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.py --skip-tags wait initialisation_provisioning.yaml"
  }
  depends_on = [
    null_resource.inventory_init
  ]
}

resource "null_resource" "recuperation_des_roles" {
  provisioner "local-exec" {
    command = "ansible-galaxy install -r ansible_role_requierement.yaml"
  }
  depends_on = [
    null_resource.provisionning_init
  ]
}

resource "null_resource" "provisionning_cluster" {
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.py -e 'main_server_ip=${var.instance.ip}.10' -e 'node_master_ip=${var.instance.ip}.11' --tags provisionning_cluster provisionning.yaml"
  }
  depends_on = [
    null_resource.recuperation_des_roles
  ]
}

resource "null_resource" "initialisation_du_cluster" {
  # Définir la connexion SSH
  connection {
    type        = "ssh"
    user        = "ndf" # L'utilisateur pour la connexion SSH
    # private_key = file("/path/to/private/key.pem") # Clé privée pour SSH
    host        = "${var.instance.ip}.10" # Adresse IP de votre serveur
  }

  # Utiliser remote-exec pour exécuter une commande Bash
  provisioner "remote-exec" {
    inline = [
      "#!/bin/bash",
      "cd /home/ndf/kubespray",
      "source venv/bin/activate",
      "ansible-playbook --private-key /home/ndf/.ssh/id_ecdsa_ndf -i inventory/cluster_ndf/inventory.ini --become --become-user=root cluster.yml"
    ]
  }
  depends_on = [
    null_resource.provisionning_cluster
  ]
}

resource "null_resource" "provisionning_outils" {
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.py -e 'main_server_ip=${var.instance.ip}.10' -e 'node_master_ip=${var.instance.ip}.11' --tags provisionning_outils provisionning.yaml"
  }
  depends_on = [
    null_resource.initialisation_du_cluster
  ]
}
